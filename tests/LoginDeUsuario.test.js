import homePage from '../pages/home.page';
import autenticacionPage from '../pages/autenticacion.page';
import successAuthPage from '../pages/successAuth.page';
import USUARIO from '../datos/usuario';
import MENSAJESEXITO from '../datos/mensajesExito';

describe('Login con una cuenta de usuario', () => {
    it('Debería iniciar sesion con una cuenta de usuario', async () => {
        await homePage.abrir('/');

        await homePage.ingresarAAutenticacion();

        await autenticacionPage.rellenarDatosLogin(USUARIO.email, USUARIO.password);

        await expect(await successAuthPage.obtenerTextoMensaje()).to.equal(MENSAJESEXITO.mensajeLoginExitoso);

    });
});