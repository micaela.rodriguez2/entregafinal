import homePage from '../pages/home.page';
import busquedaPage from '../pages/busqueda.page';
import articuloPage from '../pages/articulo.page';
import DATOS from '../datos/articulos';

describe('Ver el detalle de una prenda del catálogo', () => {
    it('Debería buscar "Blouse" y entrar al detalle', async () => {

        await homePage.abrir('/');
        
        await homePage.buscar(DATOS[0].articulo);

        await expect(await homePage.obtenerTextoBusqueda()).to.equal(articulo);

        await expect(await busquedaPage.obtenerNombreResultado()).to.equal(articulo);

        await busquedaPage.seleccionarElArticuloParaVerDetalle();

        await expect(await articuloPage.obtenerNombreDelArticulo()).to.equal(articulo);

    });
});