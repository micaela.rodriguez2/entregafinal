//En este test se aplica data driven testing

import homePage from '../pages/home.page';
import busquedaPage from '../pages/busqueda.page';
import DATOS from '../datos/articulos';

 describe('Buscar una prenda en la caja de búsqueda', function () {
   DATOS.forEach(({articulo}) => {
     it(`Debería buscar ${articulo}`, async ()=> {
       await homePage.abrir('/');

       await homePage.buscar(articulo);

       await expect(await homePage.obtenerTextoBusqueda()).to.equal(articulo);
       
       await expect(await busquedaPage.obtenerNombreResultado()).to.equal(articulo);
     });
   });
 });