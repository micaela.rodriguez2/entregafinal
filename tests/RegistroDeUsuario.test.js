import homePage from '../pages/home.page';
import autenticacionPage from '../pages/autenticacion.page';
import successAuthPage from '../pages/successAuth.page';
import registroPage from '../pages/registro.page';
import USUARIO from '../datos/usuario';
import MENSAJESEXITO from '../datos/mensajesExito';

describe('Crear una cuenta de usuario', () => {
    it('Debería crear una cuenta de usuario', async () => {
        await homePage.abrir('/');

        await homePage.ingresarAAutenticacion();

        await autenticacionPage.rellenarEmailRegistro(USUARIO.email);

        await registroPage.seleccionarGenero(USUARIO.gender);

        await registroPage.rellenarDatosBasicos(USUARIO.firstName, USUARIO.lastName, USUARIO.password);

        await registroPage.seleccionarFechaNacimiento(USUARIO.day, USUARIO.month, USUARIO.year);

        await registroPage.rellenarDatosExtras(USUARIO.company, USUARIO.address, USUARIO.extraAddress, 
            USUARIO.city, USUARIO.state, USUARIO.zipCode, USUARIO.country, USUARIO.description, USUARIO.homePhone, USUARIO.mobilePhone, USUARIO.alias);

        await registroPage.finalizarRegistro();

        await expect(await successAuthPage.obtenerTextoMensaje()).to.equal(MENSAJESEXITO.mensajeLoginExitoso);
    });
});