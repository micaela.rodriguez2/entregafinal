//En este test se incorpora la regresion visual

import homePage from '../pages/home.page';
import contactoPage from '../pages/contacto.page';
import autenticacionPage from '../pages/autenticacion.page';
import successAuthPage from '../pages/successAuth.page';
import USUARIO from '../datos/usuario';
import MENSAJECONTACTO from '../datos/mensajeContacto';
import MENSAJESEXITO from '../datos/mensajesExito';

describe('Contactar a la tienda mandando un mensaje', () => {
    it('Debería enviar un mensaje a la tienda', async () => {

        await homePage.abrir('/');

        await homePage.ingresarAAutenticacion();

        await autenticacionPage.rellenarDatosLogin(USUARIO.email, USUARIO.password);

        await expect(await successAuthPage.obtenerTextoMensaje()).to.equal(MENSAJESEXITO.mensajeLoginExitoso);

        await homePage.ingresarAContacto();

        await contactoPage.rellenarLosCampos(MENSAJECONTACTO.motivo, MENSAJECONTACTO.descripcion);

        await contactoPage.enviarFormulario();

        await expect(await contactoPage.obtenerTextoMensaje()).to.equal(MENSAJESEXITO.mensajeContactoExitoso);

        await $('div#center_column').waitForDisplayed();
        expect(await browser.checkElement(await $('div#center_column'), 'div-formulario-contacto', {})).to.equal(0);
    });
});