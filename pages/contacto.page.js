import BasePage from '../pages/base.page';

class ContactoPage extends BasePage{

    //WebElements
    get selectHeading() { return $('#id_contact')}
    get areaMensaje() { return $('#message')}
    get botonEnviar() { return $('#submitMessage')}
    get mensajeFinal() { return $('p.alert')}
    
    //-------------------------------------------------------------------------------------------------------//

    /**
     * Rellenar los campos del formulario de contacto
     */
     async rellenarLosCampos(motivo, mensaje) {
        addStep(`Seleccionar el motivo de consulta: ${motivo}`)
        await this.selectHeading.selectByAttribute('value', motivo);

        addStep(`Rellenar el mensaje: ${mensaje}`)
        await super.vaciarCampoYEnviarTexto(this.areaMensaje, mensaje);
    }

    /**
     * Enviar el formulario
     */
     async enviarFormulario() {
        addStep(`Enviar el formulario: `)
        await super.clickearElemento(this.botonEnviar);
    }

    /**
     * Obtener texto del mensaje que se muestra luego de enviar el mensaje
     */
     async obtenerTextoMensaje() {
        addStep('Obtener texto del mensaje que se muestra')
        return await this.mensajeFinal.getText();
    }
}
export default new ContactoPage();