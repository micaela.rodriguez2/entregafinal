import BasePage from './base.page';

class SuccessAuth extends BasePage {

    //WebElements
    get mensajeExito() { return $('p.info-account') }

    //-------------------------------------------------------------------------------------------------------//

    /**
     * Obtener texto del mensaje que se muestra luego de registrarse o iniciar sesión
     */
     async obtenerTextoMensaje() {
        addStep('Obtener texto del mensaje que se muestra')
        return await this.mensajeExito.getText();
    }
}
export default new SuccessAuth();