import BasePage from '../pages/base.page';

class ArticuloPage extends BasePage{

    //WebElements
    get articulo() { return $('h1')}

    //-------------------------------------------------------------------------------------------------------//

    /**
     * Obtener nombre del articulo
     */
     async obtenerNombreDelArticulo() {
        addStep(`Obtener el nombre del articulo`)
        return await this.articulo.getText();
    }
}
export default new ArticuloPage();