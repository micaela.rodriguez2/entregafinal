const PAGE_TIMEOUT = 50000
import BasePage from '../pages/base.page';

class RegistroPage extends BasePage {

    //Elementos Web
    get genderM() { return $('#id_gender1') }
    get genderF() { return $('#id_gender2') }
    get firstName() { return $('#customer_firstname') }
    get lastName() { return $('#customer_lastname') }
    get password() { return $('#passwd') }
    get selectDay() { return $('#days') }
    get selectMonth() { return $('#months') }
    get selectYear() { return $('#years') }
    get company() { return $('#company') }
    get address() { return $('#address1') }
    get extraAddress() { return $('#address2') }
    get city() { return $('#city') }
    get selectState() { return $('#id_state') }
    get zipCode() { return $('#postcode') }
    get selectCountry() { return $('#id_country') }
    get description() { return $('#other') }
    get homePhone() { return $('#phone') }
    get mobilePhone() { return $('#phone_mobile') }
    get alias() { return $('#alias') }

    get botonRegistrar() { return $('[name="submitAccount"]') }

    //-------------------------------------------------------------------------------------------------------//

    /**
     * Selecciona el genero
     */
    async seleccionarGenero(gender) {
        await this.genderF.waitForClickable({ timeout: PAGE_TIMEOUT });
        await this.genderM.waitForClickable({ timeout: PAGE_TIMEOUT });
        
        addStep(`Seleccionar genero ${gender}:`)
        if (gender === 'F') {
            await super.clickearElemento(this.genderF);
        }
        else {
            await super.clickearElemento(this.genderM);
        }
    }

    /**
     * Rellena los datos basicos
     */
    async rellenarDatosBasicos(fName, lName, pd) {
        addStep(`Rellenar el nombre: ${fName}`)
        await super.vaciarCampoYEnviarTexto(await this.firstName, fName);
        
        addStep(`Rellenar el apellido: ${lName}`)
        await super.vaciarCampoYEnviarTexto(await this.lastName, lName);
        
        addStep(`Rellenar el password: ${pd}`)
        await super.vaciarCampoYEnviarTexto(await this.password, pd);
    }
 
    /**
     * Selecciona la fecha de nacimiento de los tres ddl
     */
    async seleccionarFechaNacimiento(day, month, year) {
        
        addStep(`Seleccionar dia ${day}:`)
        await this.selectDay.selectByAttribute('value', day);

        addStep(`Seleccionar mes ${month}:`)
        await this.selectMonth.selectByAttribute('value', month);

        addStep(`Seleccionar year ${year}:`)
        await this.selectYear.selectByAttribute('value', year);
    }

    /**
         * Rellena los datos extras
         */
    async rellenarDatosExtras(Vcompany, Vaddress, VextraAddress, Vcity, Vstate, VzipCode, Vcountry, Vdescription, VhomePhone, VmobilePhone, Valias) {
        addStep(`Rellenar la empresa: ${Vcompany}`)
        await super.vaciarCampoYEnviarTexto(await this.company, Vcompany);
       
        addStep(`Rellenar la direccion: ${Vaddress}`)
        await super.vaciarCampoYEnviarTexto(await this.address, Vaddress);
        
        addStep(`Rellenar informacion extra de la direccion: ${VextraAddress}`)
        await super.vaciarCampoYEnviarTexto(await this.extraAddress, VextraAddress);
        
        addStep(`Rellenar la ciudad: ${Vcity}`)
        await super.vaciarCampoYEnviarTexto(await this.city, Vcity);

        addStep(`Seleccionar el estado ${Vstate}:`)
        await this.selectState.selectByAttribute('value', Vstate);

        addStep(`Rellenar el zip code: ${VzipCode}`)
        await super.vaciarCampoYEnviarTexto(await this.zipCode, VzipCode);

        addStep(`Seleccionar el pais ${Vcountry}:`)
        await this.selectCountry.selectByAttribute('value', Vcountry);

        addStep(`Rellenar la descripcion: ${Vdescription}`)
        await super.vaciarCampoYEnviarTexto(await this.description, Vdescription);
        
        addStep(`Rellenar el telefono fijo: ${VhomePhone}`)
        await super.vaciarCampoYEnviarTexto(await this.homePhone, VhomePhone);
        
        addStep(`Rellenar el telefono movil: ${VmobilePhone}`)
        await super.vaciarCampoYEnviarTexto(await this.mobilePhone, VmobilePhone);
        
        addStep(`Rellenar el alias: ${Valias}`)
        await super.vaciarCampoYEnviarTexto(await this.alias, Valias);
    }

    async finalizarRegistro(){
        await super.clickearElemento(this.botonRegistrar);
    }
}
export default new RegistroPage();