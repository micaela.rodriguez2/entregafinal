import BasePage from '../pages/base.page';

class HomePage extends BasePage {

    //WebElements
    get barraDeBusqueda() { return $('#search_query_top') }

    get linkContacto() { return $('[href="http://automationpractice.com/index.php?controller=contact"]') }

    get linkAutenticacion() { return $('[href="http://automationpractice.com/index.php?controller=my-account"]') }

    //-------------------------------------------------------------------------------------------------------//

    /**
     * Se ingresa a la pagina de autenticacion
     */
    async ingresarAAutenticacion() {
        addStep('Se ingresa a la pagina de autenticacion')
        await super.clickearElemento(this.linkAutenticacion);
    }

    /**
         * Se ingresa a la pagina de contacto
         */
    async ingresarAContacto() {
        addStep('Se ingresa a la pagina de contacto')
        await super.clickearElemento(this.linkContacto);
    }

    /**
     * Escribe el artículo en el campo de búsqueda y presiona Enter
     * @param {String} articulo que se buscará
     */
    async buscar(articulo) {
        addStep(`Buscar artículo: ${articulo}`)
        await super.vaciarCampoYEnviarTexto(await this.barraDeBusqueda, articulo);
        await this.barraDeBusqueda.keys('Enter');
    }

    /**
     * Obtener texto de la barra de búsqueda
     */
    async obtenerTextoBusqueda() {
        addStep('Obtener texto de la barra de búsqueda')
        return await this.barraDeBusqueda.getValue();
    }
}
export default new HomePage();