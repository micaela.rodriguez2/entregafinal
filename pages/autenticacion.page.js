const PAGE_TIMEOUT = 40000
import BasePage from '../pages/base.page';

class AutenticacionPage extends BasePage {

    //WebElements
    get emailRegistro() { return $('#email_create') }

    get emailLogin() { return $('#email') }

    get passwordLogin() { return $('#passwd') }

    get botonRegistro() { return $('#SubmitCreate') }

    get botonLogin() { return $('#SubmitLogin') }

    //-------------------------------------------------------------------------------------------------------//

    /**
     * Rellena los datos del login
     */
     async rellenarEmailRegistro(email) {
        await this.emailRegistro.waitForClickable({ timeout: PAGE_TIMEOUT });
        
        addStep(`Rellenar email: ${email}`)
        await super.vaciarCampoYEnviarTexto(await this.emailRegistro, email);
        await super.clickearElemento(this.botonRegistro);
    }

    /**
     * Rellena los datos del login
     */
    async rellenarDatosLogin(email, password) {
        addStep(`Rellenar email: ${email}`)
        await super.vaciarCampoYEnviarTexto(await this.emailLogin, email);

        addStep(`Rellenar password: ${password}`)
        await super.vaciarCampoYEnviarTexto(await this.passwordLogin, password);

        await super.clickearElemento(this.botonLogin);
    }

}
export default new AutenticacionPage();