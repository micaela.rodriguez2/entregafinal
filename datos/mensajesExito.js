module.exports = {
    mensajeLoginExitoso: 'Welcome to your account. Here you can manage all of your personal information and orders.',
    mensajeContactoExitoso: 'Your message has been successfully sent to our team.',
}